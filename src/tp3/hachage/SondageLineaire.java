package tp3.hachage;

/*
 * Classe de sondage linéaire (linear probing) pour la gestion de collisions
 * @see GestionCollisions
 */
public class SondageLineaire implements GestionCollisions {

	@Override
	/*
	 * Fonction qui donne le prochain index à vérifier pour l'insertion d'une
	 * valeur
	 * 
	 * @param h le hash code
	 * 
	 * @param i l'incrément
	 * 
	 * @return le nouvel index
	 */
	public int indexSuivant(int h, int i) {
		return h + i;
	}

}
