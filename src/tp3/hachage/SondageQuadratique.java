package tp3.hachage;

public class SondageQuadratique implements GestionCollisions {

	private int m_taille;

	public SondageQuadratique(int m) {
		m_taille = m;
	}

	public int indexSuivant(int h, int i) {
		int tmp;
		if ((i + 1) % 2 != 0)
			tmp = -1;
		else
			tmp = 1;
		return (h + tmp + (i / 2) * (i / 2)) % m_taille;
	}

}
