package tp3.hachage;

/**
 * Classe d'exception liée à la Hashtable
 */
public class HashTableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HashTableException(String s) {
		super(s);
	}
}
