package tp3.hachage;

import java.util.AbstractMap;
import java.util.Random;

/*
 * Classe qui implémente une table de hachage
 * @see TableSymbole
 */
public class TableHachage implements TableSymbole {

	private Case[] cases; // Tableau des cases contenant les clé/valeur
	private int nbElements = 0; // Nombre de d'éléments contenus dans la table
	private GestionCollisions gestion; // Gestionnaire de collision à utiliser

	// Entiers pour le hachage
	private int a, b;
	private int p;

	/*
	 * Constructeur qui crée une table de hachage de la taille donnée en
	 * paramètre
	 * 
	 * @param taille la taille de la table de hachage
	 */
	public TableHachage(int taille) {
		this(taille, new SondageLineaire());
	}

	/*
	 * Constructeur qui créer une table de hachage avec la taille spécifiée, et
	 * le gestionnaire de collision spécifié
	 * 
	 * @param taille la taille de la table de hachage
	 * 
	 * @param gestion la fonction de gestion de collisions
	 */
	public TableHachage(int taille, GestionCollisions gestion) {
		this.gestion = gestion;
		cases = new Case[taille * 2];

		Random randomeur = new Random(); // Génère des nombres aléatoires
		p = 1073676287; // Grand nombre premier
		// Sécurisation du programme avec facteurs aléatoires pour la
		// compression
		a = (Math.abs(randomeur.nextInt()) % p) + 1;
		b = Math.abs(randomeur.nextInt()) % p;
	}

	@Override
	/*
	 * Fonction qui permet de savoir si la clé en paramètre est contenue de la
	 * table
	 * 
	 * @param cle la clé à trouvé
	 * 
	 * @return true si la clé est dans la table
	 */
	public synchronized boolean contient(int cle) {
		Case caseVoulue = cases[getIndexCase(cle)];
		return (caseVoulue != null && !caseVoulue.isLibre());
	}

	@Override
	/*
	 * Fonction qui donne le nombre d'élément dans la table
	 * 
	 * @return le nombre d'élément
	 */
	public int size() {
		return nbElements;
	}

	@Override
	/*
	 * Fonction qui permet d'inséré une couple clé/valeur dans la table
	 * 
	 * @param cle la clé
	 * 
	 * @param valeur la valeur
	 */
	public synchronized void inserer(int cle, Object valeur)
			throws HashTableException {
		if (cases.length == nbElements) // Si table pleine
			throw new HashTableException(
					"Impossible d'insérer une élément : la table est pleine.");

		int index = getIndexCase(cle); // Index de la paire

		boolean doitIncrementer = true;

		if (cases[index] == null) // Si emplacement est vide
			cases[index] = new Case(new AbstractMap.SimpleEntry(cle, valeur)); // on
																				// insére
																				// le
																				// couple
		else // Si emplacement occupé ou considéré libre
		{
			doitIncrementer = cases[index].isLibre(); // On vérifie si on doit
														// incrémenter le nombre
														// d'éléments contenus,
														// i.e si la case est
														// libre
			cases[index].setValeur(valeur); // On remplace la valeur
		}

		if (doitIncrementer)
			nbElements++; // On augmente le nombre d'élément stockés

		notifyAll();
	}

	@Override
	/*
	 * Fonction qui permet d'obtenir la valeur de la clé en paramètre
	 * 
	 * @param cle la clé dont on veut la valeur correspondante
	 * 
	 * @return la valeur correspondante
	 */
	public synchronized Object get(int cle) throws HashTableException {

		if (nbElements == 0) {
			try {
				wait(); // bloque jusqu'a un notify()
			} catch (InterruptedException e) {
			}
			;
		}

		Case caseVoulue = cases[getIndexCase(cle)]; // Case où il devrait y
													// avoir la valeur

		if (caseVoulue != null && !caseVoulue.isLibre()) // Si la case est
															// occupée
			return caseVoulue.getValeur(); // On retourne la valeur
		else
			// Sinon la recherche a �chou�
			throw new HashTableException("Clé non trouvée dans la table");
	}

	@Override
	/*
	 * Fonction qui permet de supprimer un élément de la table
	 * 
	 * @param cle la clé de l'élément à supprimer
	 */
	public synchronized void supprimer(int cle) throws HashTableException {
		if (nbElements == 0) // si le table est vide
			throw new HashTableException("Table vide.");

		Case caseASupprimer = cases[getIndexCase(cle)]; // On récupère la case à
														// supprimer

		if (caseASupprimer == null || caseASupprimer.isLibre()) // Si la case
																// est déjà vide
																// ou libre
			throw new HashTableException("Clé non trouvée dans la table");

		caseASupprimer.vider(); // on vide la case
		nbElements--; // on enlève un élément
	}

	/*
	 * Fonction de hachage qui selon la chaine en pramètre donne l'indice
	 * correspondant dans la table
	 * 
	 * @param s la chaine à hacher
	 */
	private int hash(String s) {
		int h = 0;

		// hachage
		for (int i = 0; i < s.length(); i++) // On parcourt tous les caractères
		{
			h = (h << 5) | (h >>> 27); // on décale de 5 bits
			h += (int) s.charAt(i);
		}

		// on compresse pour être dans les bons indices
		return (Math.abs(a * h + b) % p) % cases.length;

	}

	/*
	 * Fonction qui permet de trouver l'indice de la case selon la clé en
	 * paramètre
	 * 
	 * @param cle la clé de la case
	 * 
	 * @return l'indice de la case, retourne null si la clé n'existe pas
	 */
	private int getIndexCase(Object cle) {
		int h = hash(new Integer((Integer) cle).toString()); // Calcul du hach
																// code
		int index = h; // index sera l'indice de la clé

		// on parcourt le tableau tant qu'on a pas trouvé la clé ou case vide
		for (int i = 0; cases[index] != null
				&& !cases[index].getCle().equals(cle); i++)
			index = gestion.indexSuivant(h, i) % cases.length; // On récupère la
																// prochaine
																// case

		return index;
	}
}
