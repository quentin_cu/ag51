package tp3.hachage;

/**
 * Interface d'une table de symbole
 */
public interface TableSymbole {

	/*
	 * Fonction qui permet d'inséré une couple clé/valeur dans la table
	 * 
	 * @param cle la clé
	 * 
	 * @param valeur la valeur
	 */
	public void inserer(int cle, Object valeur) throws HashTableException;

	/*
	 * Fonction qui permet d'obtenir la valeur de la clé en paramètre
	 * 
	 * @param cle la clé dont on veut la valeur correspondante
	 * 
	 * @return la valeur correspondante
	 */
	public Object get(int cle) throws HashTableException;

	/*
	 * Fonction qui permet de supprimer un élément de la table
	 * 
	 * @param cle la clé de l'élément à supprimer
	 */
	public void supprimer(int cle) throws HashTableException;

	/*
	 * Fonction qui permet de savoir si la clé en paramètr est contenue de la
	 * table
	 * 
	 * @param cle la clé à trouvé
	 * 
	 * @return true si la clé est dans la table
	 */
	public boolean contient(int cle);

	/*
	 * Fonction qui donne le nombre d'élément dans la table
	 * 
	 * @return le nombre d'élément
	 */
	public int size();

}
