package tp3.hachage;

import java.util.Map;

/**
 * Classe Case qui permet de stocker un couple clé/valeur et gère les
 * suppressions
 */
public class Case {

	private Map.Entry emplacement; // couple clé/valeur
	private boolean libre = true; // indique si la case est libre ou non

	/*
	 * Constructeur de la case
	 * 
	 * @param e Une paire clé/valeur
	 */
	public Case(Map.Entry e) {
		this.emplacement = e;
		this.libre = false;
	}

	/*
	 * Fonction qui permet de supprimer la case i.e. la marque comme libre
	 */
	public void vider() {
		emplacement.setValue(null);
		libre = true;
	}

	/*
	 * Fonction qui donne de la clé du contenue dans la case
	 * 
	 * @return Clé de la case
	 */
	public Object getCle() {
		return emplacement.getKey();
	}

	/*
	 * Fonction qui retourne la valeur contenue dans la case
	 * 
	 * @return valeur de la case
	 */
	public Object getValeur() {
		return emplacement.getValue();
	}

	/*
	 * Fonction qui permet de savoir si la case est libre
	 * 
	 * @return true sir bucket libre
	 */
	public boolean isLibre() {
		return libre;
	}

	/*
	 * Fonction qui permet de changer la valeur contenue dans la case
	 * 
	 * @param valeur la nouvelle valeur de la case
	 */
	public void setValeur(Object valeur) {
		emplacement.setValue(valeur);
		libre = false;
	}

}
