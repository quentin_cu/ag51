package tp3.hachage;

/*
 * Interface de gestion de collisions
 */
public interface GestionCollisions {

	/*
	 * Fonction qui donne le prochain index à vérifier pour l'insertion d'une
	 * valeur
	 * 
	 * @param h le hash code
	 * 
	 * @param i l'incrément
	 * 
	 * @return le nouvel index
	 */
	public int indexSuivant(int h, int i);
}
