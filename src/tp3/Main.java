package tp3;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Vector;

import constants.CONST;
import tp3.hachage.TableHachage;

public class Main {

	public static void main(String[] args) throws Exception {

		String NomFichier = CONST.default_path + "tp3.txt";

		try {

			PrintWriter out = new PrintWriter(new FileWriter(NomFichier));
			System.out.println("Start");

			// On fait pour des collections de taille X
			for (int j = 5000; j <= 50000; j += 5000) {

				System.out.println("Iteration :" + j);
				float sommeTemps = 0;
				
				// On excute 100 fois le l'ajout et la suppression en question
				// sur la collection de taille X pour avoir une moyenne
				for (int k = 0; k < 100; k++) {

					// On lance le chrono
					long Tbegin = System.currentTimeMillis();

					// on stocke les valeurs insérées
					Vector<Integer> vec = new Vector<Integer>();

					// avec notre hashtable
					// on fait les X insertions
					TableHachage hash = new TableHachage(j);
					for (int i = 0; i < j; i++) {
						int random = (int) (Math.random() * 30000);
						hash.inserer(random, new Integer(random));
						vec.add(random); // pour garder les clés à chercher
					}

					// on recherche tous les éléments un a un
					for (int i = 0; i < j; i++) {
						hash.get(vec.elementAt(i));
					}

					// on arrête le chrono
					long Tend = System.currentTimeMillis();
					float Time = Tend - Tbegin;
					sommeTemps += Time;
				}

				// on rengistre le temps moyen
				float moyenneTemps = sommeTemps / 100;
				out.println(j + ";" + moyenneTemps);

			}

			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
