package tp2;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Vector;

import constants.CONST;
import tp2.file.TasBinaire;

public class Main {

	public static void main(String[] args) throws Exception {

		String NomFichier = CONST.default_path + "tp2.txt";

		try {
			
			PrintWriter out = new PrintWriter(new FileWriter(NomFichier));
			System.out.println("Start");
			// On fait pour des collections de taille 1000 à 19000
			
			for (int j = 10000; j <= 200000; j += 10000) {
				
				System.out.println("Iteration :" + j);
				float sommeTemps = 0;
				// On excute 100 fois le l'ajout et la suppression en question
				// sur la collection de taille X pour avoir une moyenne
				
				for (int k = 0; k < 100; k++) {
					// On lance le chrono
					long Tbegin = System.currentTimeMillis();

					/*
					 * //on fait les X insertions Vector<Integer> A = new
					 * Vector<Integer>(); for(int i=0;i<j;i++)
					 * A.add((int)(Math.random()*30000));
					 * 
					 * //on fait le tri triRapide(A);
					 * 
					 * //on fait les suppressions while(!A.isEmpty())
					 * A.removeElementAt(A.size()-1);
					 */

					// on fait les X insertion
					TasBinaire tb = new TasBinaire();
					for (int i = 0; i < j; i++)
						tb.inserer((int) (Math.random() * 30000));

					/*
					 * //On fait les X suppressions while(!tb.isEmpty())
					 * tb.supprimerMax( );
					 */

					// On arrête le chrono
					long Tend = System.currentTimeMillis();
					float Time = Tend - Tbegin;
					sommeTemps += Time;
				}

				// On enregistre le temps moyen pour une taille donnée
				float moyenneTemps = sommeTemps / 100;
				out.println(j + ";" + moyenneTemps);
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void triRapide(Vector<Integer> A) {
		int longueur = A.size();
		triRapide(A, 0, longueur - 1);
	}

	public static void triRapide(Vector<Integer> A, int debut, int fin) {
		if (debut < fin) {
			int positionPivot = partition(A, debut, fin);
			triRapide(A, debut, positionPivot - 1);
			triRapide(A, positionPivot + 1, fin);
		}
	}

	private static int partition(Vector<Integer> A, int debut, int fin) {

		int compt = debut;
		int pivot = A.get(debut);

		for (int i = debut + 1; i <= fin; i++) {
			if (A.get(i) < pivot) {
				compt++;
				echanger(A, compt, i);
			}
		}
		echanger(A, debut, compt);
		return (compt);

	}

	public static void echanger(Vector<Integer> A, int ind1, int ind2) {
		Integer temp = A.get(ind1);
		A.set(ind1, A.get(ind2));
		A.set(ind2, temp);
	}

}
