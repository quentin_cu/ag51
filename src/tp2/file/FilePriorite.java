package tp2.file;

/**
 * Interface pour toutes les classes qui veulent suivre le modèle d'une file de
 * priorité
 */
public interface FilePriorite {

	/**
	 * Fonction qui permet d'insérer une élément dans une file de priorité
	 * 
	 * @param x
	 *            l'objet à insérer
	 */
	void inserer(Comparable x);

	/**
	 * FOnction qui permet de retrouver le maximum dans une file de priorité
	 * 
	 * @return le maximum
	 * @throws UnderflowException
	 *             si vide
	 */
	Comparable trouverMax() throws Exception;

	/**
	 * Fonction qui permet de supprimer le maximum dans une file de priorité et
	 * de le retourner
	 * 
	 * @return le maximum
	 * @throws UnderflowException
	 *             si vide
	 */
	Comparable supprimerMax() throws Exception;

	/**
	 * Fonction qui permet de savoir si la file de priorité est vide ou non
	 * 
	 * @return true si vide, false sinon
	 */
	boolean isEmpty();

	/**
	 * Fonction qui retourne la taile de la file
	 * 
	 * @return la taille
	 */
	int size();

}
