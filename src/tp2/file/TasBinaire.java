package tp2.file;

/**
 * Classe tas binaire maximum qui permet de stocker des éléments selon les
 * principes d'un tas binaire, et par conséquent d'une file de priorité
 */
public class TasBinaire implements FilePriorite {

	private static final int CAPACITE = 100; // Capacité du tas
	private int tailleActuelle; // Taille courante
	private Comparable[] elements; // Tableau des éléments

	/**
	 * Constructeur par défaut du tas binaire La taille actuelle est de 0 mais
	 * le tableau d'éléments est créer selon la capacité
	 */
	public TasBinaire() {
		tailleActuelle = 0;
		elements = new Comparable[CAPACITE + 1];
	}

	/**
	 * Fonction qui permet d'insérer une élément dans le tas binaire
	 * 
	 * @param x
	 *            l'objet à insérer
	 */
	public void inserer(Comparable x) {
		if (tailleActuelle + 1 == elements.length) // Si on a plus de place
			doublerTaille(); // on augmente la taille du tableau

		int placeVide = ++tailleActuelle; // on récupère une place vide
		elements[0] = x; // On place l'élément a placer au tout début, zone non
							// indexée, pour servir d'arrêt

		// on place le nouvel élément dans le tas selon la règle du tas binaire
		while (x.compareTo(elements[placeVide / 2]) > 0) {
			elements[placeVide] = elements[placeVide / 2];
			placeVide /= 2;

		}
		elements[placeVide] = x;

	}

	/**
	 * FOnction qui permet de retrouver le maximum dans le tas binaire
	 * 
	 * @return le maximum
	 * @throws UnderflowException
	 *             si vide
	 */
	public Comparable trouverMax() throws Exception {
		if (isEmpty())
			throw new Exception("Tas binaire vide");
		return elements[1]; // retourne le premier élément du tas binaire qui
							// est le maximum
	}

	/**
	 * Fonction qui permet de supprimer le maximum dans le tas binaire et de le
	 * retourner
	 * 
	 * @return le maximum
	 * @throws UnderflowException
	 *             si vide
	 */
	public Comparable supprimerMax() throws Exception {
		Comparable max = trouverMax(); // On recupere le max
		elements[1] = elements[tailleActuelle--]; // On le remplace par le
													// dernier element du
													// tableau
		triApresSuppression(1); // On tri
		return max;
	}

	/**
	 * Fonction qui permet de savoir si le tas binaire est vide ou non
	 * 
	 * @return true si vide, false sinon
	 */
	public boolean isEmpty() {
		return tailleActuelle == 0;
	}

	/**
	 * Fonction qui retourne la taile du tas binaire
	 * 
	 * @return la taille
	 */
	public int size() {
		return tailleActuelle;
	}

	/**
	 * Fonction qui permet de trier le tas binaire après une suppression
	 * 
	 * @param index
	 *            l'element supprimé
	 */
	private void triApresSuppression(int index) {
		
		int fils;
		Comparable temp = elements[index];

		// on redescend les élements
		while (index * 2 <= tailleActuelle) {
			// on recupère l'indice du fils
			// Si on est pas au bout du tableau et que 
			// élément suivant plus grand que le fils
			fils = index * 2; 
			if (fils != tailleActuelle
					&& elements[fils + 1].compareTo(elements[fils]) > 0)
				fils++; // On passe au suivant
			// si le courant est plus grand que le "max" du tableau
			if (elements[fils].compareTo(temp) > 0)
				elements[index] = elements[fils]; // on remplace
			else
				break; // si aucun des deux, on a fini
			index = fils;
		}
		elements[index] = temp;
	}

	/**
	 * Fonction qui permet de doubler la taille du des éléments
	 */
	private void doublerTaille() {
		Comparable[] nouveauTableau;
		nouveauTableau = new Comparable[elements.length * 2]; // On crée le
																// tableau de
																// taille double
		for (int i = 0; i < elements.length; i++)
			// on récopie les éléments aux mêmes positions
			nouveauTableau[i] = elements[i];
		elements = nouveauTableau; // on remplace l'ancien par le nouveau
	}

	public void affiche() {
		for (int i = 1; i < elements.length; i++)
			if (elements[i] != null)
				System.out.println(elements[i]);
	}

}
