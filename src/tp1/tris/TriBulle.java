package tp1.tris;

import java.util.Vector;

/**
 *
 */
public class TriBulle extends TriBase {

	/**
	 * Fonction statique triBulle qui tri un vecteur d'entier en paramètre selon
	 * l'algorithme du tri par bulles
	 * 
	 * @param A le vecteur d'entiers à trier
	 */
	public void run(Vector<Integer> A) {
		int longueur = A.size();
		boolean inversion;

		do {
			inversion = false;
			for (int i = 0; i < longueur - 1; i++) {
				if (A.get(i) > A.get(i + 1)) {
					echanger(A, i, i + 1);
					inversion = true;
				}
			}
		} while (inversion);
	}
	
}
