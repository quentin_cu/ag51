package tp1.tris;

import java.util.Vector;

/**
 *
 */
public class TriFusion extends TriBase {

	@Override
	public void run(Vector<Integer> a) {
		this.run(a, 0, a.size());
	}
	
	/**
	 * Fonction statique triFusion qui tri un vecteur d'entiers en paramètre
	 * selon l'algorithme du tri par fusion
	 * 
	 * @param A le vecteur d'entiers à trier
	 */
	public void run(Vector<Integer> A, int commencement, int fin) {
		if (commencement < fin) {
			int milieu = (commencement + fin) / 2;
			run(A, commencement, milieu);
			run(A, milieu + 1, fin);
			fusionner(A, commencement, milieu, fin);
		}
	}
	
	/**
	 * Fonction qui permet de fusionner deux triées d'un vecteur
	 * 
	 * @param A le vecteur d'entier à demi trié
	 * @param commencement l'indice du début de la première partie trié du
	 *            vecteur
	 * @param milieu l'indicie de la fin de la première partie trié du vecteur
	 * @param fin l'indice de la fin de la deuxième partir trié du vecteur
	 */
	public void fusionner(Vector<Integer> A, int commencement,
			int milieu, int fin) {

		int fin1 = milieu;
		int debut2 = milieu + 1;
		while ((commencement <= fin1) && (debut2 <= fin)) {
			if (A.get(commencement) < A.get(debut2)) {
				commencement++;
			} else {
				int temp = A.get(debut2);
				for (int k = debut2 - 1; k >= commencement; k--) {
					A.set(k + 1, A.get(k));
				}
				A.set(commencement, temp);
				commencement++;
				fin1++;
				debut2++;
			}
		}

	}
	
}
