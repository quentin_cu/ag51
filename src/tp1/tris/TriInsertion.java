package tp1.tris;

import java.util.Vector;

/**
 *
 */
public class TriInsertion extends TriBase {

	/**
	 * Fonction statique triInsertion qui tri un vecteur d'entier en paramètre
	 * selon l'algorithme du tri par insertion
	 * 
	 * @param A le vecteur d'entiers à trier
	 */
	public void run(Vector<Integer> A) {
		int cle, i;
		for (int j = 1; j < A.size(); j++) {
			cle = A.get(j);
			// insertion de la clé das la partie déjà triée du vecteur
			i = j - 1;
			while (i > -1 && A.get(i) > cle) {
				A.set(i + 1, A.get(i));
				i--;
			}
			A.set(i + 1, cle);
		}
	}
	
}
