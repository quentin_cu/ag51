package tp1.tris;

import java.util.Vector;

import tp1.algo.AlgoInterface;

/**
 *
 */
abstract class TriBase implements AlgoInterface {

	/**
	 * Fonction qui permet d'échanger deux éléments d'un vecteur d'entiers aux
	 * indices spécifiés
	 * 
	 * @param A Le vecteur d'entiers concernés
	 * @param ind1 indice du premier élément à échanger
	 * @param ind2 indice du deuxième élément à échanger
	 */
	public static void echanger(Vector<Integer> A, int ind1, int ind2) {
		Integer temp = A.get(ind1);
		A.set(ind1, A.get(ind2));
		A.set(ind2, temp);
	}
	
}
