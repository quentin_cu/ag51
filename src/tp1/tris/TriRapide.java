package tp1.tris;

import java.util.Vector;

/**
 *
 */
public class TriRapide extends TriBase {

	/**
	 * Fonction statique triRapide qui tri un vecteur d'entiers en paramètre
	 * selon l'algorithme du tri rapide
	 * 
	 * @param A le vecteur d'entiers à trier
	 */
	public void run(Vector<Integer> A) {
		int longueur = A.size();
		run(A, 0, longueur - 1);
	}

	/**
	 * Fonction statique triRapide qui tri un vecteur d'entiers unqiement entre
	 * deux indices en paramètre selon l'algorithme du tri rapide
	 * 
	 * @param A le vecteur d'entiers à trier
	 * @param debut l'indice de début de la zone à trier
	 * @param fin l'indice de fin de la zone à trier
	 */
	public void run(Vector<Integer> A, int debut, int fin) {
		if (debut < fin) {
			int positionPivot = partition(A, debut, fin);
			run(A, debut, positionPivot - 1);
			run(A, positionPivot + 1, fin);
		}
	}
	
	/**
	 * Fonction qui permet de définir la position du pivot pour le tri rapide
	 * sur un vecteur d'entiers entre deux indices
	 * 
	 * @param A le vecteur d'entiers à trier
	 * @param debut l'indice du début de la zone à trier
	 * @param fin l'indice de la fin de la zone à trier
	 * @return l'indice du pivot pour le tri rapide
	 */
	private static int partition(Vector<Integer> A, int debut, int fin) {

		int compt = debut;
		int pivot = A.get(debut);

		for (int i = debut + 1; i <= fin; i++) {
			if (A.get(i) < pivot) {
				compt++;
				echanger(A, compt, i);
			}
		}
		echanger(A, debut, compt);
		return (compt);

	}
	
}
