package tp1.tris;

import java.util.Vector;

/**
 *
 */
public class TriParTas extends TriBase {

	/**
	 * Fonction statique triParTas qui tri un vecteur d'entiers en paramètre
	 * selon l'algorithme du tri par tas
	 * 
	 * @param A le vecteur d'entiers à trier
	 */
	public void run(Vector<Integer> A) {
		
		int longueur = A.size();
		for (int i = (longueur / 2) - 1; i >= 0; i--) {
			tamiser(A, i, longueur - 1);
		}

		for (int i = longueur - 1; i >= 1; i--) {
			echanger(A, i, 0);
			tamiser(A, 0, i - 1);
		}
		
	}
	
	/**
	 * Fonction tamiser qui permet de tamiser un vecteur d'entiers entre deux
	 * indices
	 * 
	 * @param A le vecteur sur lequel on doit tamiser
	 * @param ind1 indice de début
	 * @param ind2 indice de fin
	 */
	public void tamiser(Vector<Integer> A, int ind1, int ind2) {
		
		int k = ind1;
		int j = 2 * k;
		
		while (j <= ind2) {
			if (j < ind2 && A.get(j) < A.get(j + 1))
				j++;
			if (A.get(k) < A.get(j)) {
				echanger(A, k, j);
				k = j;
				j = 2 * k;
			} else {
				break;
			}
		}
		
	}
	
}
