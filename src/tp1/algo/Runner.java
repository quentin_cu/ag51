package tp1.algo;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Vector;

import constants.CONST;

/**
 * 
 * @author qc
 *
 */
public class Runner {
	
	/**
	 * 
	 */
	AlgoInterface algo = null;

    /**
     *
     * @param algo AlgoInterface
     */
	public Runner(AlgoInterface algo) {
		this.algo = algo;
	}

    /**
     *
     */
	public void run() {
		this.run(CONST.default_path);
	}

    /**
     *
     * @param filePath path du ficher
     */
	public void run(String filePath) {
		try {
			
			PrintWriter out = new PrintWriter(new FileWriter(filePath));
			System.out.println("Start");
			
			// On fait pour des vector de taille 1000 ?? 19000
			for (int j = 10; j <= 100; j += 10) {
				
				System.out.println("Iteration en pour :" + j);
				float sommeTemps = 0;
				// On excute 100 fois le tri en question sur le vecteur de
				// taille X pour avoir une moyenne
				for (int k = 0; k < 100; k++) {
					
					// On cr??e un vecteur de nombres al??atoires
					Vector<Integer> tab = new Vector<Integer>();
					for (int i = 0; i < j; i++)
						tab.add((int) (Math.random() * 3000));

					// On lance le chronom??tre
					long Tbegin = System.currentTimeMillis();
					
					this.algo.run(tab);
					
					// On arr??te le chronm??tre
					long Tend = System.currentTimeMillis();
					sommeTemps += Tend - Tbegin;
				}

				// On enregistre le temp d'??excution moyen du 
				// tris pour la taille donn??e
				float moyenneTemps = sommeTemps / 100;
				out.println(j + ";" + moyenneTemps);

			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
