package tp1;

import tp1.algo.Runner;
import tp1.tris.TriRapide;

/**
 * 
 * @author qc
 *
 */
public class Main {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		
		new Runner(new TriRapide()).run();
		
		
	}
	
}
