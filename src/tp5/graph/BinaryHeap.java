/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tp5.graph;

import java.util.Vector;

/**
 * 
 * Classe qui implémente un tas binaire
 * @author qc
 *
 */
public class BinaryHeap {

	/**
	 * les données contenues
	 */
	private Vector<Comparable> data;
	
	/**
	 * La taille
	 */
	private int size;

	/**
	 * Constructeur par défaut
	 */
	public BinaryHeap() {
		size = 0;
		data = new Vector<Comparable>();
	}

	/**
	 * Constructeur du tas à partir d'un vecteur de données
	 * 
	 * @param v le vecteur de données à mettre dans le tas
	 */
	public BinaryHeap(Vector<Comparable> v) {
		data = v;
		this.size = v.size();

		System.out.println("Avant heapify :");
		affiche();

		for (int i = (size - 1) / 2; i >= 0; i--) {
			heapify(i);
		}

		System.out.println("Après heapify :");
		affiche();
	}

	/**
	 * Fonction qui permet de remettre le tas en bon ordre
	 * 
	 * @param ind l'indice depuis lequel on part
	 */
	public void heapify(int ind) {
		int l = getLeftChildIndex(ind);
		int r = getRightChildIndex(ind);
		int min;

		if (l < size && data.get(l).compareTo(data.get(ind)) < 0) {
			min = l;
		} else {
			min = ind;
		}

		if (r < size && data.get(r).compareTo(data.get(min)) < 0) {
			min = r;
		}

		if (min != ind) {
			echanger(min, ind);
			heapify(min);
		}
	}

	/**
	 * Fonction qui permet d'éhcnager deux valeurs contenues dans le ts
	 * 
	 * @param a la première valeur
	 * @param b la seconde valeur
	 */
	public void echanger(int a, int b) {
		Comparable temp = data.get(a);
		data.set(a, data.get(b));
		data.set(b, temp);
	}

	/**
	 * Fonction qui permet d'insérer un élément dans le tas
	 * 
	 * @param e l'élément à insérer
	 */
	public void inserer(Comparable e) {
		size++;
		data.setSize(size);
		int i = size - 1;

		while (i > 0 && e.compareTo(data.get(getParentIndex(i))) < 0) {
			data.set(i, data.get(getParentIndex(i)));
			i = getParentIndex(i);
		}

		data.set(i, e);
	}

	/**
	 * Fonction qui donne la valeur minimale du tas
	 * 
	 * @return la valeur minimale
	 */
	public Comparable minimum() {
		if (isEmpty())
			throw new HeapException("Heap is empty");
		else
			return data.get(0);
	}

	/**
	 * Fonction qui supprime et retourne la valeur minimale du tas
	 * 
	 * @return la valeur minimale
	 */
	public Comparable extraire_min() {
		Comparable max = data.get(0);
		data.set(0, data.get(size - 1));
		size--;
		data.setSize(size);
		heapify(0);
		return max;
	}

	/**
	 * Fonction qui permet de savoir si le tas est vide
	 * 
	 * @return true s'il est vide
	 */
	public boolean isEmpty() {
		return (size == 0);
	}

	/**
	 * Fonction qui donne l'index du fils gauche d'un élément selon son indice
	 * 
	 * @param nodeIndex indice du père
	 * @return indice du fils gauche
	 */
	private int getLeftChildIndex(int nodeIndex) {
		return 2 * nodeIndex + 1;
	}

	/**
	 * Fonction qui donne l'index du fils droit d'un élément selon son indice
	 * 
	 * @param nodeIndex indice du père
	 * @return indice du fils droit
	 */
	private int getRightChildIndex(int nodeIndex) {
		return 2 * nodeIndex + 2;
	}

	/**
	 * Fonction qui donne l'index du père d'un élément selon son indice
	 * 
	 * @param nodeIndex indice du fils
	 * @return indice du père
	 */
	private int getParentIndex(int nodeIndex) {
		return (nodeIndex - 1) / 2;
	}

	/**
	 * Fonction qui permet d'afficher le tas
	 */
	public void affiche() {
		for (int i = 0; i < size; i++) {
			System.out.print(data.get(i) + "\t");
		}
		System.out.println("\n");
	}

	/**
	 * Classe d'exception du tas
	 */
	public class HeapException extends RuntimeException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public HeapException(String message) {
			super(message);
		}
	}
	
}