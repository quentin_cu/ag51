/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tp5;

import java.io.FileWriter;
import java.io.PrintWriter;

import tp5.graph.DirectedGraph;
import constants.CONST;

public class Main {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {

		String NomFichier = CONST.default_path + "tp5.txt";

		try {

			PrintWriter out = new PrintWriter(new FileWriter(NomFichier));
			System.out.println("Start");

			// On fait pour des vector de taille 1000 à 19000
			for (int j = 1000; j <= 10000; j += 1000) {

				System.out.println("Iteration :" + j);
				float sommeTemps = 0;
				// On excute 100 fois l'algo

				for (int k = 0; k < 100; k++) {

					// On crée un digraph de j vertices
					DirectedGraph G = new DirectedGraph();
					for (int i = 0; i < j; i++) {

						// on fait deux arcs par sommet
						int dest = i;
						while (dest == i) {
							// pour pas le relier à lui même
							dest = (int) (Math.random() * j);
						}
						G.addEdge("" + i, "" + dest, (int) (Math.random() * 20));

						dest = i;
						while (dest == i) {
							// pour pas le relier à lui même
							dest = (int) (Math.random() * j);
						}
						G.addEdge("" + i, "" + dest, (int) (Math.random() * 20));

					}

					// On lance le chronomètre
					long Tbegin = System.currentTimeMillis();

					// On lance l'algo
					// int source = (int) (Math.random() * j);
					// G.DijkstraWithBinaryHeap(""+source);
					// G.DijkstraWithFibonnaciHeap(""+source);
					G.topologicalSort();

					// On arrête le chronmètre
					long Tend = System.currentTimeMillis();
					float Time = Tend - Tbegin;
					sommeTemps += Time;

				}

				// On enregistre le temp d'éexcution moyen du tris pour la
				// taille donnée
				float moyenneTemps = sommeTemps / 100;
				out.println(j + ";" + moyenneTemps);

			}

			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
